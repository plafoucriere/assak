# Assak Changelog

## v0.3.1
- Fix importing from multiple files

## v0.3
- New command `assak validate`: Validate vulnerabilities in YAML files

## v0.2
- Use a BASE image with a shell to run in GitLab-CI

## v0.1
- New command `assak import`: Import vulnerabilities through YAML files.
