package main

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/kylelemons/godebug/pretty"
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestImport(t *testing.T) {

	firstFile := `---
- category: sast
  name: vuln1
  message: Message
  description: a long description
  cve: todo
  severity: high
  location:
    file: ./main.go
    start_line: 18
    end_line: 18

- category: sast
  name: vuln2
  message: Message 2 
  description: a long description again
  cve: todo
  severity: low
  location:
    file: ./main.go
    start_line: 12
    end_line: 12`

	secondFile := `---
- category: sast
  name: vuln3
  message: Message 3
  description: a long description again
  cve: todo
  severity: low
  location:
    file: ./main.go
    start_line: 24
    end_line: 24`

	expectedReport := issue.NewReport()
	expectedReport.Vulnerabilities = []issue.Issue{
		{
			Category:    issue.CategorySast,
			Name:        "vuln1",
			Message:     "Message",
			Description: "a long description",
			CompareKey:  "todo",
			Severity:    issue.SeverityLevelHigh,
			Location:    issue.Location{File: "./main.go", LineStart: 18, LineEnd: 18},
		},
		{
			Category:    issue.CategorySast,
			Name:        "vuln3",
			Message:     "Message 3",
			Description: "a long description again",
			CompareKey:  "todo",
			Severity:    issue.SeverityLevelLow,
			Location:    issue.Location{File: "./main.go", LineStart: 24, LineEnd: 24},
		},
		{
			Category:    issue.CategorySast,
			Name:        "vuln2",
			Message:     "Message 2",
			Description: "a long description again",
			CompareKey:  "todo",
			Severity:    issue.SeverityLevelLow,
			Location:    issue.Location{File: "./main.go", LineStart: 12, LineEnd: 12},
		},
	}
	addScanner(&expectedReport)

	dir, err := ioutil.TempDir("", "example")
	if err != nil {
		log.Fatal(err)
	}
	defer os.RemoveAll(dir)

	fp := filepath.Join(dir, "vulnerabilities.yml")

	if err := ioutil.WriteFile(fp, []byte(firstFile), 0666); err != nil {
		log.Fatal(err)
	}

	fp = filepath.Join(dir, "vulnerabilities-2.yml")

	if err := ioutil.WriteFile(fp, []byte(secondFile), 0666); err != nil {
		log.Fatal(err)
	}

	t.Run("Simple glob", func(t *testing.T) {
		report := issue.NewReport()
		err := Import([]string{filepath.Join(dir, "*.yml")}, &report)
		if err != nil {
			t.Fatal(err)
		}

		if diff := pretty.Compare(report, expectedReport); diff != "" {
			t.Errorf("Imported report mismatch: (-got +want)\n%s", diff)
		}
	})

	t.Run("Empty glob", func(t *testing.T) {
		// Empty glob defaults to "*.yml", so we need to run the test in the temporary directory
		err := os.Chdir(filepath.Join(dir))
		if err != nil {
			panic(err)
		}

		report := issue.NewReport()
		err = Import([]string{}, &report)
		if err != nil {
			t.Fatal(err)
		}

		if diff := pretty.Compare(report, expectedReport); diff != "" {
			t.Errorf("Imported report mismatch: (-got +want)\n%s", diff)
		}
	})

	t.Run("Invalid glob", func(t *testing.T) {
		report := issue.NewReport()
		err := Import([]string{"[-]"}, &report)
		if err != filepath.ErrBadPattern {
			t.Errorf("Import should fail with ErrBadPattern, got '%v'", err)
		}
	})

	t.Run("No files found", func(t *testing.T) {
		report := issue.NewReport()
		err := Import([]string{"foo"}, &report)
		if err.Error() != errors.New("Couldn't find any file matching '[foo]'").Error() {
			t.Errorf("Import should report no files found, but got: %s", err)
		}
	})
}
