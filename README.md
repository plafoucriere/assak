# ASSAK

Assak, the AppSec Swiss Army Knife, is a tool used by the GitLab AppSec team to interact with Security Reports.

## Usage

The best way to use this software is to use the provided Docker image:

```sh
$ docker run -it --rm registry.gitlab.com/plafoucriere/assak -h
```

## Contribute

See the [CONTRIBUTING.md](CONTRIBUTING.md) file.
